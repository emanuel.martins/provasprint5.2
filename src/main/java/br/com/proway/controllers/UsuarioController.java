package br.com.proway.controllers;

import br.com.proway.models.Usuario;
import br.com.proway.repositories.UsuarioRepository;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("")
@AllArgsConstructor // Boas praticas ao declarar a injeção de dependência de um repositório
public class UsuarioController {

    private final UsuarioRepository usuarioRepository;

    @PostMapping("/users")
    public ResponseEntity<Object> addUsuario(@RequestBody Usuario userBody) {
        Usuario novoUsuario;

        try {
            novoUsuario = (Usuario) usuarioRepository.save(userBody);

            return new ResponseEntity<>(novoUsuario, HttpStatus.CREATED);
        } catch (Exception e) {
            System.err.println(e.getLocalizedMessage());

            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/users")
    public ResponseEntity<List<Object>> getAllUsuarios() {
        try {
            return new ResponseEntity(usuarioRepository.findAll(), HttpStatus.OK);
        } catch (Exception exception) {
            System.err.println(exception.getMessage());

            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/users/{emailDomain}")
    public ResponseEntity<Object> getAllUsersByEmailDomain(@PathVariable("emailDomain") String emailDomain) {
        try {
            return new ResponseEntity<>(usuarioRepository.getUserBySpecificEmail(emailDomain), HttpStatus.OK);
        } catch (Exception e) {
            System.err.println(e.getLocalizedMessage());

            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        }
    }
}
