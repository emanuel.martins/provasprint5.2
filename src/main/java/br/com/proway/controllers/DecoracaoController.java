package br.com.proway.controllers;

import br.com.proway.dto.DecoracaoDTO;
import br.com.proway.models.Decoracao;
import br.com.proway.repositories.DecoracaoRepository;
import br.com.proway.repositories.UsuarioRepository;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("")
@AllArgsConstructor
public class DecoracaoController {

    private final DecoracaoRepository decoracaoRepository;
    private final UsuarioRepository usuarioRepository;

    @PostMapping("/decoracoes/{emailFuncionario}")
    public ResponseEntity<Object> atualizarServico(@RequestBody Decoracao decoracaoBody,
                                                   @PathVariable("emailFuncionario") String emailFuncionario) {
        Decoracao decoracao;

        try {
            decoracaoBody.setUsuario(usuarioRepository.findByEmail(emailFuncionario));
            decoracao = decoracaoRepository.save(decoracaoBody);

            return new ResponseEntity<>(decoracao, HttpStatus.CREATED);
        } catch (Exception e) {
            System.err.println(e.getLocalizedMessage());

            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/decoracoes")
    public ResponseEntity<List<Object>> getAllDecoracoes() {
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");

        try {
            ArrayList<DecoracaoDTO> listaDecoracoes = new ArrayList<>();

            for (Decoracao decoracao : decoracaoRepository.findAll()) {
                listaDecoracoes.add(
                        new DecoracaoDTO(decoracao.getId(), decoracao.getTipo(),
                                simpleDateFormat.format(decoracao.getData()),
                                decoracao.getImagem(), decoracao.getUsuario())
                );
            }

            return new ResponseEntity(listaDecoracoes, HttpStatus.OK);
        } catch (Exception exception) {
            System.err.println(exception.getMessage());

            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/decoracoes/{tipo}")
    public ResponseEntity<Object> getServicosPeloTipo(@PathVariable("tipo") String tipo) {
        try {
            return new ResponseEntity(decoracaoRepository.findPorTipoServico(tipo), HttpStatus.OK);
        } catch (Exception exception) {
            System.err.println(exception.getMessage());

            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }
}
