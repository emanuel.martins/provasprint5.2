package br.com.proway.enums;

public enum Cargo {
    ATENDENTE,
    SECRETARIA,
    DESIGNER
}
