package br.com.proway.models;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@ToString
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "decoracoes")
public class Decoracao {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;

    @Column
    private String tipo;

    @Column(nullable = false)
    private Date data;

    @Column
    private String imagem;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "fk_id_usuario")
    private Usuario usuario;
}
