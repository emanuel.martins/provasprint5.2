package br.com.proway.dto;

import br.com.proway.models.Usuario;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.UUID;

@Data
@AllArgsConstructor
public class DecoracaoDTO {
    private UUID id;
    private String tipo;
    private String data;
    private String imagem;
    private Usuario usuario;
}
