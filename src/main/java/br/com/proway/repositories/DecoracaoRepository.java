package br.com.proway.repositories;

import br.com.proway.models.Decoracao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface DecoracaoRepository extends JpaRepository<Decoracao, UUID> {
    Decoracao save(Decoracao servico);
    Optional<Decoracao> findById(UUID id);
    ArrayList<Decoracao> findAll();

    @Query("SELECT d FROM Decoracao d WHERE d.tipo LIKE CONCAT('%', :tipo ,'%')")
    ArrayList<Decoracao> findPorTipoServico(String tipo);
}
