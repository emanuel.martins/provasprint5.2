package br.com.proway.repositories;

import br.com.proway.models.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.UUID;

@Repository
public interface UsuarioRepository extends JpaRepository<Usuario, UUID> {
    ArrayList<Usuario> findAll();
    Usuario save(Usuario usuario);
    Usuario findByEmail(String email);

    @Query("SELECT u FROM Usuario u WHERE u.email LIKE CONCAT('%', :mail ,'%')")
    ArrayList<Usuario> getUserBySpecificEmail(String mail);
}
