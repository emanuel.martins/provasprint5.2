--
-- PostgreSQL database dump
--

-- Dumped from database version 14.4
-- Dumped by pg_dump version 14.4

-- Started on 2022-07-06 13:55:21

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 3 (class 2615 OID 2200)
-- Name: public; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA public;


ALTER SCHEMA public OWNER TO postgres;

--
-- TOC entry 3320 (class 0 OID 0)
-- Dependencies: 3
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA public IS 'standard public schema';


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 210 (class 1259 OID 17368)
-- Name: decoracoes; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.decoracoes (
    id uuid NOT NULL,
    data timestamp without time zone NOT NULL,
    imagem character varying(255),
    tipo character varying(255),
    fk_id_usuario uuid
);


ALTER TABLE public.decoracoes OWNER TO postgres;

--
-- TOC entry 209 (class 1259 OID 17341)
-- Name: usuarios; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.usuarios (
    id uuid NOT NULL,
    cargo integer,
    email character varying(255) NOT NULL,
    nome character varying(255) NOT NULL
);


ALTER TABLE public.usuarios OWNER TO postgres;

--
-- TOC entry 3314 (class 0 OID 17368)
-- Dependencies: 210
-- Data for Name: decoracoes; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.decoracoes (id, data, imagem, tipo, fk_id_usuario) FROM stdin;
58edcc6e-fd14-48be-a7eb-e6af0f1ed384	2022-06-06 21:00:00	https://cdn.pixabay.com/photo/2014/07/10/17/17/bedroom-389254_960_720.jpg	Decoração de Quarto	03f1d093-934c-4514-a01f-a213a7bd9aad
f0197efc-6e68-42d4-abfe-701402340ad4	2022-06-06 21:00:00	https://cdn.pixabay.com/photo/2016/04/18/08/50/kitchen-1336160_960_720.jpg	Decoração de Cozinha e Copa	03f1d093-934c-4514-a01f-a213a7bd9aad
2107dd50-0e55-4086-be83-5f45f0b3ec04	2022-06-06 21:00:00	https://cdn.pixabay.com/photo/2018/06/22/17/54/casino-3491252_960_720.jpg	Decoração de Salão de Jogos	b5ffe1ae-4e17-4a56-bf8d-c4ec8f9e2bdf
76a264a1-ab1f-478f-91f2-0b2aea3871cd	2022-06-06 21:00:00	https://cdn.pixabay.com/photo/2017/02/07/18/16/living-room-2046668_960_720.jpg	Decoração de Sala de Estar	b5ffe1ae-4e17-4a56-bf8d-c4ec8f9e2bdf
\.


--
-- TOC entry 3313 (class 0 OID 17341)
-- Dependencies: 209
-- Data for Name: usuarios; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.usuarios (id, cargo, email, nome) FROM stdin;
af212913-103f-473f-b94d-217e48186829	2	pedro@bing.com.br	Pedro
03f1d093-934c-4514-a01f-a213a7bd9aad	2	emanuel@gmail.com	Emanuel
a16c63cb-b248-4998-8fd5-caa3d2ac852d	2	barbara@mail.com	Barbara
b5ffe1ae-4e17-4a56-bf8d-c4ec8f9e2bdf	2	ju@outlook.com	Julia
346aff76-201f-496f-b63e-fe82d9edb38d	0	luan.aa@email.com	Luana
b16b4f21-f6bc-4b2f-ba06-37b7d95c9234	2	lu.cc@email.com	Luciana
\.


--
-- TOC entry 3172 (class 2606 OID 17374)
-- Name: decoracoes decoracoes_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.decoracoes
    ADD CONSTRAINT decoracoes_pkey PRIMARY KEY (id);


--
-- TOC entry 3168 (class 2606 OID 17349)
-- Name: usuarios uk_kfsp0s1tflm1cwlj8idhqsad0; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuarios
    ADD CONSTRAINT uk_kfsp0s1tflm1cwlj8idhqsad0 UNIQUE (email);


--
-- TOC entry 3170 (class 2606 OID 17347)
-- Name: usuarios usuarios_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuarios
    ADD CONSTRAINT usuarios_pkey PRIMARY KEY (id);


--
-- TOC entry 3173 (class 2606 OID 17375)
-- Name: decoracoes fkt5efb1qp84d1j0q07mxeekovy; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.decoracoes
    ADD CONSTRAINT fkt5efb1qp84d1j0q07mxeekovy FOREIGN KEY (fk_id_usuario) REFERENCES public.usuarios(id);


-- Completed on 2022-07-06 13:55:21

--
-- PostgreSQL database dump complete
--

